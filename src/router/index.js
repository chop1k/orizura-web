import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "@/views/Home";

Vue.use(VueRouter)

const routes = [
  {
    'name': 'home',
    'path': '/',
    'component': Home
  },
  {
    'name': 'bookmarks',
    'path': '/b/:id',
    'component': () => import('@/views/Bookmarks/Bookmarks.vue'),
    'props': true
  },
  {
    'name': 'login',
    'path': '/u/login',
    'component': () => import('@/views/Login.vue')
  },
  {
    'name': 'register',
    'path': '/u/register',
    'component': () => import('@/views/Register.vue')
  },
  {
    'name': 'not-found',
    'path': '*',
    'component': () => import('@/views/NotFound.vue')
  }
]

const router = new VueRouter({
  routes,
  'mode': 'history',
  'base': process.env.BASE_URL
})

export default router
