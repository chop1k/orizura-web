import {serverUrl} from "@/store";

export default {
    'state': {

    },
    'getters': {

    },
    'actions': {
        async register(context, payload) {
            let response = await fetch(`${serverUrl}/u/register`, {
                'method': 'POST',
                'mode': 'cors',
                'credentials': 'include',
                'headers': {
                    'Content-Type': 'application/json'
                },
                'body': JSON.stringify({
                    'name': payload.name,
                    'email': payload.email,
                    'password': payload.password
                })
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred during registration.',
                    'text': await response.text()
                })
            } else {
                await context.dispatch('authenticate')
            }
        },
    },
    'mutations': {

    }
}