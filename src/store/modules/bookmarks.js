import {apiUrl} from "@/store";
import Vue from 'vue'

export default {
    'state': {
        bookmarks: [
        ],
        directories: [
        ],
    },
    'getters': {
        bookmarks(state) {
            return state.bookmarks
        },
        directories(state) {
            return state.directories
        }
    },
    'actions': {
        async fetchBookmarks(context, payload) {
            let response = await fetch(`${apiUrl}/b/${payload.id}`, {
                'method': 'GET',
                'mode': 'cors',
                'credentials': 'include',
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while getting bookmarks.',
                    'text': await response.text()
                })
            } else {
                context.commit('setBookmarks', await response.json())
            }
        },
        async fetchDirectories(context, payload) {
            let response = await fetch(`${apiUrl}/d/${payload.id}`, {
                'method': 'GET',
                'mode': 'cors',
                'credentials': 'include',
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while getting directories.',
                    'text': await response.text()
                })
            } else {
                context.commit('setDirectories', await response.json())
            }
        },
        async createBookmark(context, bookmark) {
            let response = await fetch(`${apiUrl}/b/create`, {
                'method': 'POST',
                'mode': 'cors',
                'credentials': 'include',
                'headers': {
                    'content-type': 'application/json'
                },
                'body': JSON.stringify(bookmark)
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while creating a new bookmark.',
                    'text': await response.text()
                })
            } else {
                bookmark.id = await response.json().id

                context.commit('addBookmark', bookmark)
            }
        },
        async createDirectory(context, directory) {
            let response = await fetch(`${apiUrl}/d/create`, {
                'method': 'POST',
                'mode': 'cors',
                'credentials': 'include',
                'headers': {
                    'content-type': 'application/json'
                },
                'body': JSON.stringify(directory)
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while creating a new directory.',
                    'text': await response.text()
                })
            } else {
                let json = await response.json()

                directory.id = json.id // TODO: check id exists

                context.commit('addDirectory', directory)
            }
        },
        async deleteBookmark(context, id) {
            let response = await fetch(`${apiUrl}/b/${id}/delete`, {
                'method': 'DELETE',
                'mode': 'cors',
                'credentials': 'include'
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while deleting the bookmark.',
                    'text': await response.text()
                })
            } else {
                context.commit('removeBookmark', id)
            }
        },
        async deleteDirectory(context, id) {
            let response = await fetch(`${apiUrl}/d/${id}/delete`, {
                'method': 'DELETE',
                'mode': 'cors',
                'credentials': 'include'
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while deleting the directory.',
                    'text': await response.text()
                })
            } else {
                context.commit('removeDirectory', id)
            }
        },
        async editBookmark(context, bookmark) {
            let response = await fetch(`${apiUrl}/b/${bookmark.id}/edit`, {
                'method': 'POST',
                'mode': 'cors',
                'credentials': 'include',
                'headers': {
                    'content-type': 'application/json'
                },
                'body': JSON.stringify({
                    'name': bookmark.name,
                    'url': bookmark.url,
                    'description': bookmark.description,
                    'directory': bookmark.directory
                })
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while editing the bookmark.',
                    'text': await response.text()
                })
            } else {
                context.commit('editBookmark', bookmark)
            }
        },
        async editDirectory(context, directory) {
            let response = await fetch(`${apiUrl}/d/${directory.id}/edit`, {
                'method': 'POST',
                'mode': 'cors',
                'credentials': 'include',
                'headers': {
                    'content-type': 'application/json'
                },
                'body': JSON.stringify({
                    'name': directory.name,
                    'parent': directory.parent
                })
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while editing the directory.',
                    'text': await response.text()
                })
            } else {
                context.commit('editDirectory', directory)
            }
        }
    },
    'mutations': {
        addBookmark(state, bookmark) {
            state.bookmarks.push(bookmark)
        },
        addDirectory(state, directory) {
            state.directories.push(directory)
        },
        setBookmarks(state, bookmarks) {
            state.bookmarks = bookmarks
        },
        setDirectories(state, directories) {
            state.directories = directories
        },
        removeBookmark(state, id) {
            state.bookmarks = state.bookmarks.filter((b) => b.id !== id)  // TODO: find another way to delete from array
        },
        removeDirectory(state, id) {
            state.directories = state.directories.filter((d) => d.id !== id)

            state.bookmarks = state.bookmarks.filter((b) => b.directory === id)
        },
        editBookmark(state, bookmark) {
            Vue.set(state.bookmarks, state.bookmarks.findIndex((b) => b.id === bookmark.id), bookmark) // TODO: -1 index check
        },
        editDirectory(state, directory) {// TODO: -1 index check
            Vue.set(state.directories, state.directories.findIndex((b) => b.id === directory.id), directory)
        }
    }
}