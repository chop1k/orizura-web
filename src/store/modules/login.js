import {serverUrl} from "@/store";

export default {
    'state': {

    },
    'getters': {

    },
    'actions': {
        async login(context, payload) {
            let response = await fetch(`${serverUrl}/u/login`, {
                'method': 'POST',
                'mode': 'cors',
                'credentials': 'include',
                'headers': {
                    'Content-Type': 'application/json'
                },
                'body': JSON.stringify({
                    'name': payload.name,
                    'password': payload.password
                })
            })

            if (!response.ok) {
                context.commit('addAlert', {
                    'header': 'An exception occurred while logging in.',
                    'text': await response.text()
                })
            } else {
                await context.dispatch('authenticate')
            }
        },
    },
    'mutations': {

    }
}