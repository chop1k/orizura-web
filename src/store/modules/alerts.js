export default {
    'state': {
        'alerts': [
        ]
    },
    'getters': {
        alerts(state) {
            return state.alerts
        }
    },
    'actions': {
    },
    'mutations': {
        addAlert(state, alert) {
            alert.id = new Date().getTime()

            state.alerts.push(alert)
        },
        removeAlert(state, id) {
            state.alerts = state.alerts.filter((a) => a.id !== id)
        }
    }
}