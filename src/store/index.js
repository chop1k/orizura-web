import Vue from 'vue'
import Vuex from 'vuex'
import login from './modules/login'
import register from './modules/register'
import bookmarks from './modules/bookmarks'
import alerts from "./modules/alerts";

Vue.use(Vuex)

export const serverUrl = 'http://127.0.0.1:8890'
export const apiUrl = 'http://127.0.0.1:8889'

export default new Vuex.Store({
  'state': {
    'token': {},
    'loading': true
  },
  'mutations': {
    setToken(state, token) {
      state.loading = false

      state.token = {
        'user': token.user,
        'app': token.app
      }
    }
  },
  'actions': {
    async authenticate(context) {
      let response = await fetch(`${apiUrl}/t/get`, {
        'method': 'GET',
        'mode': 'cors',
        'credentials': 'include',
        'headers': {
          'Content-Type': 'application/json'
        },
      })

      if (!response.ok) {
        context.commit('addAlert', {
          'header': 'An exception occurred during authentication.',
          'text': await response.text()
        })
      } else {
        context.commit('setToken', await response.json())
      }
    }
  },
  'getters': {
    token(state) {
      return state.token
    },
    loading(state) {
      return state.loading
    }
  },
  'modules': {
    login, register, bookmarks, alerts
  }
})
