FROM node:latest as build-stage

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx as production-stage

COPY --from=build-stage /app /var/www/orizura.org/
COPY --from=build-stage /app/docker/deployment/nginx.conf /etc/nginx/nginx.conf